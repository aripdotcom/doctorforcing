#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings('ignore')

import numpy as np
import getopt
import sys
import os
import math
import time
import argparse
from visdom import Visdom
import random

sys.path.insert(0, os.path.join('..', '..'))

import torch as T
from torch.autograd import Variable as var
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data
from torchvision import datasets, transforms
from torch.autograd import Variable as var

from torch.nn.utils import clip_grad_norm

from dnc.dnc import DNC
from dnc.sdnc import SDNC
from dnc.sam import SAM
from dnc.util import *
from dnc.forcing_dnc import FDNC
from modules.discriminator import Discriminator 

parser = argparse.ArgumentParser(description='PyTorch Differentiable Neural Computer')
parser.add_argument('-input_size', type=int, default=1, help='dimension of input feature')
parser.add_argument('-rnn_type', type=str, default='gru', help='type of recurrent cells to use for the controller')
parser.add_argument('-nhid', type=int, default=64, help='number of hidden units of the inner nn')
parser.add_argument('-dropout', type=float, default=0, help='controller dropout')
parser.add_argument('-memory_type', type=str, default='fdnc', help='dense or sparse memory: dnc | sdnc | sam | fdnc')

parser.add_argument('-nlayer', type=int, default=1, help='number of layers')
parser.add_argument('-nhlayer', type=int, default=1, help='number of hidden layers default was 2')
parser.add_argument('-lr', type=float, default=1e-4, help='initial learning rate')
parser.add_argument('-optim', type=str, default='adam', help='learning rule, supports adam|rmsprop')
parser.add_argument('-clip', type=float, default=50, help='gradient clipping')

parser.add_argument('-batch_size', type=int, default=100, metavar='N', help='batch size')
parser.add_argument('-mem_size', type=int, default=20, help='memory dimension')
parser.add_argument('-mem_slot', type=int, default=16, help='number of memory slots')
parser.add_argument('-read_heads', type=int, default=4, help='number of read heads')
parser.add_argument('-sparse_reads', type=int, default=10, help='number of sparse reads per read head')
parser.add_argument('-temporal_reads', type=int, default=2, help='number of temporal reads')

parser.add_argument('-sequence_max_length', type=int, default=4, metavar='N', help='sequence_max_length')
parser.add_argument('-curriculum_increment', type=int, default=0, metavar='N', help='sequence_max_length incrementor per 1K iterations')
parser.add_argument('-curriculum_freq', type=int, default=1000, metavar='N', help='sequence_max_length incrementor per 1K iterations')
parser.add_argument('-cuda', type=int, default=2, help='Cuda GPU ID, -1 for CPU')

parser.add_argument('-iterations', type=int, default=100000, metavar='N', help='total number of iteration')
parser.add_argument('-summarize_freq', type=int, default=10, metavar='N', help='summarize frequency')
parser.add_argument('-check_freq', type=int, default=100, metavar='N', help='check point frequency')
parser.add_argument('-visdom', action='store_true', help='plot memory content on visdom per -summarize_freq steps')
parser.add_argument('-pf', type=int, default=0,  help='Use Professor forcing')
parser.add_argument('-df', type=int, default=1,  help='Use Doctor Forcing')

args = parser.parse_args()
print(args)

if args.pf:
    args.df=0
if args.df:
    args.pf=0

viz = Visdom()
# assert viz.check_connection()

if args.cuda != -1:
  print('Using CUDA.')
  T.manual_seed(1111)
else:
  print('Using CPU.')


def llprint(message):
  sys.stdout.write(message)
  sys.stdout.flush()


def generate_data(batch_size, length, size, cuda=-1):

  input_data = np.zeros((batch_size, 2 * length + 1, size), dtype=np.float32)
  target_output = np.zeros((batch_size, 2 * length + 1, size), dtype=np.float32)

  sequence = np.random.binomial(1, 0.5, (batch_size, length, size - 1))

  input_data[:, :length, :size - 1] = sequence
  input_data[:, length, -1] = 1  # the end symbol
  target_output[:, length + 1:, :size - 1] = sequence

  input_data = T.from_numpy(input_data)
  target_output = T.from_numpy(target_output)
  if cuda != -1:
    input_data = input_data.cuda()
    target_output = target_output.cuda()

  return var(input_data), var(target_output)


#Learn about optional loss
def calculate_generator_loss(tf_scores, ar_scores, optional_loss=False):
    loss = torch.log(ar_scores) * (-1)

    if optional_loss:
        loss += torch.log(1 - tf_scores) * (-1)
    return loss

def calcualte_discriminator_loss(tf_scores, ar_scores):
    tf_loss = torch.log(tf_scores) * (-1)
    ar_loss = torch.log(1 - ar_scores) * (-1)
    return tf_loss + ar_loss


class GENDATA():
    def __init__(self, batch_size, size, cuda=1):
        self.batch_size = batch_size
        self.size = size
        self.device = cuda
        train_loader = torch.utils.data.DataLoader(
                       datasets.MNIST('/home/vtretyak/projects/data/mnist', train=True, download=True,
                               transform=transforms.Compose([
                               transforms.ToTensor(),])),
                        batch_size=self.batch_size, shuffle=True)

        test_loader = torch.utils.data.DataLoader(
                          datasets.MNIST('/home/vtretyak/projects/data/mnist', train=False, download=True,
                              transform=transforms.Compose([
                              transforms.ToTensor(),])),
                          batch_size=self.batch_size, shuffle=True)
        
        self.len_train_loader = len(train_loader)
        self.len_test_loader = len(test_loader)
        
        shape = (self.len_train_loader , self.batch_size, 28*28+2, 1)
        data_train_pixel = torch.zeros(shape)
        data_train_pixel_target = torch.zeros(shape)
        
        test_shape = (self.len_test_loader, self.batch_size, 28*28+2, 1)
        data_test_pixel = torch.zeros(test_shape)
        data_test_pixel_target = torch.zeros(test_shape)

        for idx, (data, target) in enumerate(train_loader):
            #convert to sequence of len 28*28 by 1 pixel
            data_pixel = data.view((self.batch_size, 28*28, 1))
            data_train_pixel[idx][:,2:,:] = data_pixel
            data_train_pixel_target[idx][:,1:-1,:] = data_pixel
            
        for idx, (data, target) in enumerate(test_loader):
            #convert to sequence of len 28*28 by 1 pixel
            data_pixel = data.view((self.batch_size, 28*28, 1))
            data_test_pixel[idx][:,2:,:] = data_pixel
            data_test_pixel_target[idx][:,1:-1,:] = data_pixel

        self.data_train_pixel = data_train_pixel.to(self.device)
        self.data_train_pixel_target = data_train_pixel_target.to(self.device)
        self.data_test_pixel = data_test_pixel.to(self.device)
        self.data_test_pixel_target = data_test_pixel_target.to(self.device)
    
    def sample_batch(self):
        choose = random.randint(0, self.len_train_loader/self.batch_size)
        sample = self.data_train_pixel[choose]
        sample_target = self.data_train_pixel_target[choose]
        return var(sample), var(sample_target)
    
    def get_test(self):
        return var(self.data_test_pixel), var(self.data_test_pixel_target)
        

def criterion(predictions, targets):
  return T.mean(-1 * F.logsigmoid(predictions) * (targets) - T.log(1 - F.sigmoid(predictions) + 1e-9) * (1 - targets))

def log_softmax(x): 
    return x - x.exp().sum(-1).log().unsqueeze(-1)

def nll(input, target): 
    return -input[range(target.shape[0]), target].mean()

test_criterion = nn.CrossEntropyLoss()


if __name__ == '__main__':

  dirname = os.path.dirname(__file__)
  ckpts_dir = os.path.join(dirname, 'checkpoints')
  if not os.path.isdir(ckpts_dir):
    os.mkdir(ckpts_dir)

  batch_size = args.batch_size
  sequence_max_length = args.sequence_max_length
  iterations = args.iterations
  summarize_freq = args.summarize_freq
  check_freq = args.check_freq

  # input_size = output_size = args.input_size
  mem_slot = args.mem_slot
  mem_size = args.mem_size
  read_heads = args.read_heads

  if args.memory_type == 'dnc':
    rnn = DNC(
        input_size=args.input_size,
        hidden_size=args.nhid,
        rnn_type=args.rnn_type,
        num_layers=args.nlayer,
        num_hidden_layers=args.nhlayer,
        dropout=args.dropout,
        nr_cells=mem_slot,
        cell_size=mem_size,
        read_heads=read_heads,
        gpu_id=args.cuda,
        debug=args.visdom,
        batch_first=True,
        independent_linears=True
    )
  elif args.memory_type == 'fdnc':
    rnn = FDNC(
        input_size=args.input_size,
        hidden_size=args.nhid,
        rnn_type=args.rnn_type,
        num_layers=args.nlayer,
        num_hidden_layers=args.nhlayer,
        dropout=args.dropout,
        nr_cells=mem_slot,
        cell_size=mem_size,
        read_heads=read_heads,
        gpu_id=args.cuda,
        debug=args.visdom,
        batch_first=True,
        independent_linears=True
    )
  elif args.memory_type == 'sdnc':
    rnn = SDNC(
        input_size=args.input_size,
        hidden_size=args.nhid,
        rnn_type=args.rnn_type,
        num_layers=args.nlayer,
        num_hidden_layers=args.nhlayer,
        dropout=args.dropout,
        nr_cells=mem_slot,
        cell_size=mem_size,
        sparse_reads=args.sparse_reads,
        temporal_reads=args.temporal_reads,
        read_heads=args.read_heads,
        gpu_id=args.cuda,
        debug=args.visdom,
        batch_first=True,
        independent_linears=False
    )
  elif args.memory_type == 'sam':
    rnn = SAM(
        input_size=args.input_size,
        hidden_size=args.nhid,
        rnn_type=args.rnn_type,
        num_layers=args.nlayer,
        num_hidden_layers=args.nhlayer,
        dropout=args.dropout,
        nr_cells=mem_slot,
        cell_size=mem_size,
        sparse_reads=args.sparse_reads,
        read_heads=args.read_heads,
        gpu_id=args.cuda,
        debug=args.visdom,
        batch_first=True,
        independent_linears=False
    )  
  else:
    raise Exception('Not recognized type of memory')
     
  gen_data_class = GENDATA(batch_size, args.input_size, args.cuda)

  print(rnn)
  print(args.nhid)
  # register_nan_checks(rnn)

  if args.pf:
      H_discriminator = Discriminator(
        args.nhid,#opt.hidden_size,
        512,#opt.d_hidden_size,
        512,#opt.d_linear_size,
        0.2,#opt.d_dropout,
        args.cuda, #opt.device  args.cuda
      )
      print(H_discriminator)
    
  
  if args.df:
      H_discriminator = Discriminator(
        args.nhid,#opt.hidden_size input size,
        512,#opt.d_hidden_size,
        512,#opt.d_linear_size,
        0.2,#opt.d_dropout,
        args.cuda, #opt.device  args.cuda
      )
      print(H_discriminator)
    
      M_discriminator = Discriminator(
        20,#input size,
        512,#opt.d_hidden_size,
        512,#opt.d_linear_size,
        0.2,#opt.d_dropout,
        args.cuda, #opt.device  args.cuda
      )
      print(M_discriminator)

  if args.cuda != -1:
    #ON CUDA NOW!
    rnn = rnn.cuda(args.cuda)
    if args.pf:
        H_discriminator = H_discriminator.cuda(args.cuda)
    if args.df:
        H_discriminator = H_discriminator.cuda(args.cuda)
        M_discriminator = M_discriminator.cuda(args.cuda)

  last_save_nll_losses = []

  if args.optim == 'adam':
    optimizer = optim.Adam(rnn.parameters(), lr=args.lr, eps=1e-9, betas=[0.9, 0.98]) # 0.0001
    g_optimizer = optim.Adam(rnn.parameters(), lr=args.lr, eps=1e-9, betas=[0.9, 0.98]) # 0.0001
  elif args.optim == 'adamax':
    optimizer = optim.Adamax(rnn.parameters(), lr=args.lr, eps=1e-9, betas=[0.9, 0.98]) # 0.0001
    g_optimizer = optim.Adamax(rnn.parameters(), lr=args.lr, eps=1e-9, betas=[0.9, 0.98]) # 0.0001
  elif args.optim == 'rmsprop':
    optimizer = optim.RMSprop(rnn.parameters(), lr=args.lr, momentum=0.9, eps=1e-10) # 0.0001
    g_optimizer = optim.RMSprop(rnn.parameters(), lr=args.lr, momentum=0.9, eps=1e-10) # 0.0001
  elif args.optim == 'sgd':
    optimizer = optim.SGD(rnn.parameters(), lr=args.lr) # 0.01
    g_optimizer = optim.SGD(rnn.parameters(), lr=args.lr) # 0.01
  elif args.optim == 'adagrad':
    optimizer = optim.Adagrad(rnn.parameters(), lr=args.lr)
    g_optimizer = optim.Adagrad(rnn.parameters(), lr=args.lr)
  elif args.optim == 'adadelta':
    optimizer = optim.Adadelta(rnn.parameters(), lr=args.lr)
    g_optimizer = optim.Adadelta(rnn.parameters(), lr=args.lr)
    
  if args.pf:
      hd_optimizer = optim.Adam(H_discriminator.parameters(), lr=args.lr)
   
  if args.df:
      hd_optimizer = optim.Adam(H_discriminator.parameters(), lr=args.lr)
      md_optimizer = optim.Adam(M_discriminator.parameters(), lr=args.lr)


  (chx, mhx, rv) = (None, None, None)
  for epoch in range(iterations + 1):
    llprint("\rIteration {ep}/{tot}".format(ep=epoch, tot=iterations))
    optimizer.zero_grad()

    random_length = np.random.randint(1, sequence_max_length + 1)

    input_data, target_output = gen_data_class.sample_batch()

    if rnn.debug:
      output, (chx, mhx, rv), v = rnn(input_data, (None, mhx, None), reset_experience=True, pass_through_memory=True)
    else:
      output, (chx, mhx, rv) = rnn(input_data, (None, mhx, None), reset_experience=True, pass_through_memory=True)
    
    g_output, (g_chx, g_mhx, g_rv) = rnn.generate(input_data, (None, mhx, None), reset_experience=True, pass_through_memory=True)

    
    nll_loss = criterion((output), target_output)
    nll_loss.backward(retain_graph=True)
    T.nn.utils.clip_grad_norm(rnn.parameters(), args.clip)
    optimizer.step()
    nll_loss_value = nll_loss.item()
    
    
    if args.pf:
        teacher_forcing_scores = H_discriminator(chx[0])
        autoregressive_scores = H_discriminator(g_chx[0])
        d_loss = calcualte_discriminator_loss(teacher_forcing_scores, autoregressive_scores).sum()
        g_loss = calculate_generator_loss(teacher_forcing_scores, autoregressive_scores).sum()
        print(teacher_forcing_scores)
        print(autoregressive_scores)
        d_loss.backward(retain_graph=True)
        g_loss.backward()
        g_optimizer.step()
        hd_optimizer.step()
        
    if args.df:
        #Hidden state is a vector
        #print(chx[0].size())
        teacher_forcing_scores = H_discriminator(chx[0])
        autoregressive_scores = H_discriminator(g_chx[0])
        d_loss = calcualte_discriminator_loss(teacher_forcing_scores, autoregressive_scores).sum()
        g_loss = calculate_generator_loss(teacher_forcing_scores, autoregressive_scores).sum()
        #print('Hidden state teacher_forcing_scores:',teacher_forcing_scores)
        #print('Hidden state autoregressive_scores:',autoregressive_scores)
        d_loss.backward(retain_graph=True)
        g_loss.backward(retain_graph=True)
        g_optimizer.step()
        hd_optimizer.step()
        
        #Memory is a vector
        #print(mhx['memory'].size())
        memory_teacher_forcing_scores = M_discriminator(mhx['memory'])
        memory_autoregressive_scores = M_discriminator(g_mhx['memory'])
        md_loss = calcualte_discriminator_loss(memory_teacher_forcing_scores, memory_autoregressive_scores).sum()
        mg_loss = calculate_generator_loss(memory_teacher_forcing_scores, memory_autoregressive_scores).sum()
        #print('Memory teacher_forcing_scores:', memory_teacher_forcing_scores)
        #print('Memory memory_autoregressive_scores:', memory_autoregressive_scores)
        md_loss.backward(retain_graph=True)
        mg_loss.backward()
        g_optimizer.step()
        md_optimizer.step()
    
    #g_loss_value = g_loss.item()
    #d_loss_value = d_loss.item()

    summarize = (epoch % summarize_freq == 0)
    test_NLL = (epoch % summarize_freq == 0) 
    take_checkpoint = (epoch != 0) and (epoch % check_freq == 0)
    increment_curriculum = (epoch != 0) and (epoch % args.curriculum_freq == 0)

    # detach memory from graph
    mhx = { k : (v.detach() if isinstance(v, var) else v) for k, v in mhx.items() }
    
    last_save_nll_losses.append(nll_loss_value)
    #last_save_losses += loss_value
    

    if summarize:
      nll_loss = np.mean(last_save_nll_losses)
      llprint("\n\tAvg. Logistic Loss: %.4f\n" % (nll_loss))
      #last_save_losses = 0
      if np.isnan(nll_loss):
        raise Exception('nan Loss')
        
    if test_NLL:
        NLL = []
        input_data, target_output = gen_data_class.get_test()
        for inp, tar in zip(input_data[:2], target_output[:2]):
            out, (chx, mhx, rv) = rnn(inp, (None, mhx, None), reset_experience=True, pass_through_memory=True)
            test_loss_value=0
            
            for idx in range(len(out[0][:])):
                test_loss = criterion(out[:,idx,:], tar[:,idx,:])
                test_loss_value += test_loss.item()
                NLL.append(test_loss_value)
            print(np.mean(NLL))
            mhx = { k : (v.detach() if isinstance(v, var) else v) for k, v in mhx.items() }
        print(np.mean(NLL))

    if summarize and rnn.debug:
      nll_loss = np.mean(last_save_nll_losses)
      last_save_nll_losses = []

      if args.memory_type == 'dnc':
        viz.heatmap(
            v['memory'],
            opts=dict(
                xtickstep=10,
                ytickstep=2,
                title='Memory, t: ' + str(epoch) + ', loss: ' + str(nll_loss),
                ylabel='layer * time',
                xlabel='mem_slot * mem_size'
            )
        )

      if args.memory_type == 'dnc':
        viz.heatmap(
            v['link_matrix'][-1].reshape(args.mem_slot, args.mem_slot),
            opts=dict(
                xtickstep=10,
                ytickstep=2,
                title='Link Matrix, t: ' + str(epoch) + ', loss: ' + str(nll_loss),
                ylabel='mem_slot',
                xlabel='mem_slot'
            )
        )
      elif args.memory_type == 'sdnc':
        viz.heatmap(
            v['link_matrix'][-1].reshape(args.mem_slot, -1),
            opts=dict(
                xtickstep=10,
                ytickstep=2,
                title='Link Matrix, t: ' + str(epoch) + ', loss: ' + str(nll_loss),
                ylabel='mem_slot',
                xlabel='mem_slot'
            )
        )

        viz.heatmap(
            v['rev_link_matrix'][-1].reshape(args.mem_slot, -1),
            opts=dict(
                xtickstep=10,
                ytickstep=2,
                title='Reverse Link Matrix, t: ' + str(epoch) + ', loss: ' + str(nll_loss),
                ylabel='mem_slot',
                xlabel='mem_slot'
            )
        )

      elif args.memory_type == 'sdnc' or args.memory_type == 'dnc':
        viz.heatmap(
            v['precedence'],
            opts=dict(
                xtickstep=10,
                ytickstep=2,
                title='Precedence, t: ' + str(epoch) + ', loss: ' + str(nll_loss),
                ylabel='layer * time',
                xlabel='mem_slot'
            )
        )

      if args.memory_type == 'sdnc':
        viz.heatmap(
            v['read_positions'],
            opts=dict(
                xtickstep=10,
                ytickstep=2,
                title='Read Positions, t: ' + str(epoch) + ', loss: ' + str(nll_loss),
                ylabel='layer * time',
                xlabel='mem_slot'
            )
        )

      viz.heatmap(
          v['read_weights'],
          opts=dict(
              xtickstep=10,
              ytickstep=2,
              title='Read Weights, t: ' + str(epoch) + ', loss: ' + str(nll_loss),
              ylabel='layer * time',
              xlabel='nr_read_heads * mem_slot'
          )
      )

      viz.heatmap(
          v['write_weights'],
          opts=dict(
              xtickstep=10,
              ytickstep=2,
              title='Write Weights, t: ' + str(epoch) + ', loss: ' + str(nll_loss),
              ylabel='layer * time',
              xlabel='mem_slot'
          )
      )

      viz.heatmap(
          v['usage_vector'] if args.memory_type == 'dnc' else v['usage'],
          opts=dict(
              xtickstep=10,
              ytickstep=2,
              title='Usage Vector, t: ' + str(epoch) + ', loss: ' + str(nll_loss),
              ylabel='layer * time',
              xlabel='mem_slot'
          )
      )

    if increment_curriculum:
      sequence_max_length = sequence_max_length + args.curriculum_increment
      print("Increasing max length to " + str(sequence_max_length))

    if take_checkpoint:
      llprint("\nSaving Checkpoint ... "),
      check_ptr = os.path.join(ckpts_dir, 'step_{}.pth'.format(epoch))
      cur_weights = rnn.state_dict()
      T.save(cur_weights, check_ptr)
      llprint("Done!\n")
    


  for i in range(int((iterations + 1) / 10)):
    llprint("\nIteration %d/%d" % (i, iterations))
    # We test now the learned generalization using sequence_max_length examples
    random_length = np.random.randint(2, sequence_max_length * 10 + 1)
    input_data, target_output, loss_weights = generate_data_2(random_length, input_size)

    if rnn.debug:
      output, (chx, mhx, rv), v = rnn(input_data, (None, mhx, None), reset_experience=True, pass_through_memory=True)
    else:
      output, (chx, mhx, rv) = rnn(input_data, (None, mhx, None), reset_experience=True, pass_through_memory=True)

    output = output[:, -1, :].sum().data.cpu().numpy()[0]
    target_output = target_output.sum().data.cpu().numpy()

    try:
      print("\nReal value: ", ' = ' + str(int(target_output[0])))
      print("Predicted:  ", ' = ' + str(int(output // 1)) + " [" + str(output) + "]")
    except Exception as e:
      pass

